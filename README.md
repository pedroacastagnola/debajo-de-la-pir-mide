# [Under The Pyramid](https://pcastagnola.itch.io/under-the-pyramid)

**Version 1.0.0**

<img src="Imagenes/Arte.jpg" width="500">

# DESCRIPTION
The player is an archaeologist who gets locked in the depths of a pyramid with his companions. Each archaeologist must find the clues and steal from their companions to escape.
Watch out for the blackouts! You don't know what may crawl in the dark.

<img src="Imagenes/48pcy7.gif"> <br/>
<img src="Imagenes/juego1.gif">
<img src="Imagenes/juego2.gif">

**Available on** [**itch.io**](https://pcastagnola.itch.io/under-the-pyramid)

# COPYRIGHT 2020

Design and Development by Pedro Agustín Castagnola (IG: @pedro.castagnola)

Art by Valeria Agostina Pérez (IG: @pandixartt)

