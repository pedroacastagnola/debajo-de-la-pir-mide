﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RomperBaldosa : MonoBehaviour
{
    private bool entro=false;
    void Start()
    {
        entro = false;
    }

    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && (!entro))
        {
            //Debug.Log("posicion: " + transform.position.x);
            entro = true;
            int posicion = (Mathf.Abs((int) transform.position.x+84))/2;
            if (posicion % 2 == 0)
            {
                PisoRunner.RomperBaldosa(posicion);
            }

            
            
        }
        
    }

}
