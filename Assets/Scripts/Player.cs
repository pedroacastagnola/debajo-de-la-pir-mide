﻿using System.Collections;
using System.Collections.Generic;
//using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    private float movZ;
    private float movX;
    private Vector3 movimiento;
    public float vel=4;
    public float velGiro=1.0f;
    private Animator anim;
    private Rigidbody rb;
    public int vasijas=0;
    public Text cantTexto;
    public float vida=100.0f;
    private bool aplastar=false;
    private bool aplastando = false;
    private bool correr=false;
    private bool isPiso=true;
    public float distanciaPiso = 0.1f;
    public float fuerzaAbajo = 1.0f;
    private bool terminado = false;
    public float tiempoCoolDownGiroCam = 1.0f;
    //public float tiempoCoolDownDerrota = 5.0f;
    private bool perdio=false;
    //private bool cayo = false;
    //public GameObject run;

    private void Start()
    {
        anim =GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        vasijas = 0;
        perdio = false;
        terminado = false;
        correr = false;
        cantTexto.text = vasijas.ToString();
    }

    public float getVida()
    {
        return vida;
    }

    public int getVasijas()
    {
        return vasijas;
    }

    public float restarVida()
    {
        return vida=vida-20.0f;
    }
    void Update()
    {
        //MomentoRunner momento=run.gameObject.GetComponent<MomentoRunner>();
        correr = MomentoRunner.getCorrer();
        Debug.Log("player:"+correr);

        if (!correr)
        {
            movZ = Input.GetAxis("Vertical");
            movX = Input.GetAxis("Horizontal");
            anim.SetFloat("Velocity", movZ);
            anim.SetFloat("VelocityX", movX);

            movimiento = new Vector3(movX, 0.0f, movZ);

            aplastar = Input.GetButtonDown("Jump");
            if (aplastar && movimiento == Vector3.zero)
            {
                anim.SetTrigger("Aplastar");
            }

            aplastando = anim.GetCurrentAnimatorStateInfo(0).IsName("Aplastar");
        }
        else
        {
            if (tiempoCoolDownGiroCam != 0.0f)
            {
                anim.SetFloat("Velocity", 0.0f);
                anim.SetFloat("VelocityX", 0.0f);
                movimiento = Vector3.zero;
                tiempoCoolDownGiroCam -= Time.deltaTime;
                tiempoCoolDownGiroCam = Mathf.Max(0.0f, tiempoCoolDownGiroCam);
            }
            

            if (tiempoCoolDownGiroCam == 0.0f && !terminado)
            {
                movX = Input.GetAxis("Horizontal");
                anim.SetFloat("Velocity", movX);
                anim.SetFloat("VelocityX", -1.0f);

                movimiento = new Vector3(-1.0f, 0.0f, movX);

                if (!isPiso)// && !cayo)
                {
                    movimiento = new Vector3(0.0f, -fuerzaAbajo, 0.0f);
                    //cayo = true;
                }

            }

        }

        if (Muerto())
        {
            //rb.constraints = RigidbodyConstraints.None;
            anim.SetTrigger("Morir");
            perdio = true;
            
        }

    }


    void FixedUpdate()
    {

        if (!correr)
        {


            if (rb != null && !aplastando)
            {
                if (movimiento != Vector3.zero && !aplastar)
                {
                    var rotObjetivo = Quaternion.LookRotation(movimiento);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotObjetivo, Time.deltaTime * velGiro);


                    transform.Translate(movimiento * vel * Time.deltaTime, Space.World);
                }
            }
        }
        else
        {
            transform.rotation = Quaternion.LookRotation(Vector3.left,Vector3.up) ;
            transform.Translate(movimiento * vel * Time.deltaTime, Space.World);

            perdio = VerificarCaida();
            if (perdio)
            {
                perdio = true;
            }

            if (TerminarJuego.juegoTerminado)
            {
                /*anim.SetFloat("Velocity", 0.0f);
                anim.SetFloat("VelocityX", 0.0f);
                movimiento = Vector3.zero;*/
                terminado = true;

                //Debug.Log("Termino");
            }
            

        }


    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.layer);
        if (other.gameObject.CompareTag("Vasija"))
        {
            GameObject.Destroy(other.gameObject);
            vasijas++;
            cantTexto.text = vasijas.ToString();

        }
    }

    private bool VerificarCaida()
    {
        int layerPiso = LayerMask.GetMask("piso");
        isPiso = Physics.Raycast(new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), -transform.up, distanciaPiso, layerPiso);

        if (!isPiso)
        {
            anim.SetTrigger("Caer");
            return true;
            
        }

        return false;
    }


   private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), transform.position - transform.up * distanciaPiso);
    }

    public int getLayer()
    {
        RaycastHit hit;
        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z), -transform.up, out hit, distanciaPiso))
        {
            //Debug.Log(LayerMask.LayerToName(hit.collider.gameObject.layer));
            return hit.collider.gameObject.layer;
        }

        return 0;
    }

    public bool Muerto()
    {
        if ((vida<=0))
        {
            return true;
        }

        return false;
    }

    public bool getPerdio()
    {
        return perdio;
    }

    void OnPisadaDer()
    {
        //Debug.Log("pisada derecha");
    }

    void OnPisadaIzq()
    {
       // Debug.Log("pisada izquierda");
    }

    void OnPisada()
    {
        //Debug.Log("pisada");
    }

}
