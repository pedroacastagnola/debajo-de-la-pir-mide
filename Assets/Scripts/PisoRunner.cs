﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PisoRunner : MonoBehaviour
{
    public int filas=30;
    public int columnas=3;
    public GameObject prefabBloque;
    public GameObject prefabRomperBloque;
    private static GameObject[,] matriz;
    public GameObject bloquePared;
    public GameObject techo;
    public GameObject esc;
    //public GameObject prefabTerminarJuego;
    void Start()
    {
        matriz = new GameObject[filas, columnas];
        var posicion = new Vector3(-83,-4,-6);

        for (int i=0; i < filas; i++)
        {
            for (int j=0;j<columnas;j++)
            {
                matriz[i,j]=GameObject.Instantiate(prefabBloque, posicion ,Quaternion.identity);
                posicion.z += 2;
                
            }
            if (i < filas - 2)
            {
                var posicionBloqueInv = new Vector3(posicion.x - 1, 1, posicion.z - 4);
                GameObject.Instantiate(prefabRomperBloque, posicionBloqueInv, Quaternion.identity);
            }

            posicion.x -= 2;
            posicion.z = -6;
        }

        ArmarParedes();
        ArmarEscaleras();
    }

    public static void RomperBaldosa(int pos)
    {
        //Debug.Log(pos);
        int posBloque = (pos +3);
        //Debug.Log("bloque: "+posBloque);
        int columnaRnd=Random.Range(0, 3);
        GameObject bloque = matriz[posBloque, columnaRnd];
        Rigidbody rb = bloque.GetComponent<Rigidbody>();
        rb.useGravity = true;
    }

    private void ArmarParedes()
    {
        for (int i = 0; i < filas; i++)
        {
            GameObject bloque=matriz[i, 0];
            var posPared1 = new Vector3(bloque.transform.position.x,0.0f,bloque.transform.position.z-2);
            GameObject.Instantiate(bloquePared, posPared1, Quaternion.identity);

            GameObject bloque2 = matriz[i, columnas-1];
            var posPared2= new Vector3(bloque2.transform.position.x, 0.0f, bloque2.transform.position.z + 2);
            GameObject.Instantiate(bloquePared, posPared2, Quaternion.identity);

            var posTecho= new Vector3(bloque.transform.position.x, 5.0f, -12.0f);
            GameObject.Instantiate(techo, posTecho, techo.transform.rotation);
        }
    }

    private void ArmarEscaleras()
    {
        GameObject bloque = matriz[filas-1, 0];
        //Debug.Log(bloque.gameObject.transform.position);
        //var posEscaleras = new Vector3(bloque.transform.position.x-9, -0.1f, -4.0f);
        var posEscaleras = new Vector3(bloque.transform.position.x + 371.3f, -15.0f, -48.0f);
        //Debug.Log(posEscaleras);
        GameObject.Instantiate(esc, posEscaleras, esc.transform.rotation);

        //var posicionFinal = posEscaleras;//new Vector3(esc.transform.position.x - 3, esc.transform.position.y, esc.transform.position.z);
        //GameObject.Instantiate(prefabTerminarJuego, posicionFinal, prefabTerminarJuego.transform.rotation);
    }

}
