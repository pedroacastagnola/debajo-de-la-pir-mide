﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TerminarJuego : MonoBehaviour
{
    public static bool juegoTerminado=false;

    // Update is called once per frame

    private void Start()
    {
        juegoTerminado = false;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            juegoTerminado = true;
            //Debug.Log("entroCollider");
            //Application.LoadLevel("Ganar");
            SceneManager.LoadScene("Ganar");
        } 
    }
}
