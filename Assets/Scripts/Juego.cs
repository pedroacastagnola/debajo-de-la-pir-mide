﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Juego : MonoBehaviour
{

    public GameObject prefabVasija;
    public GameObject prefabAranaAux;
    private int cantVasijas = 3;
    private int cantAranas = 3;
    private Vector3[] vasijas;
    private Vector3[] aranas;
    private Vector3[] aranas2;
    private float dist = 100.0f;

    public GameObject sala2;
    public GameObject sala1;
    private MeshRenderer s1;
    private MeshRenderer s2;

    public Player player;
    public GameObject salida;
    public GameObject puertaSalida;
    private static bool gano = false;
    private float distSeparacion = 10.0f;

    private bool creadasSala2 = false;

    public float tiempoCoolDownDerrota = 5.0f;

    private static bool perdio = false;

    public Text gameover;
    public Button menu;


    void Start()
    {
        gano = false;
        creadasSala2 = false;
        perdio = false;
        s1 = sala1.GetComponent<MeshRenderer>();
        s2 = sala2.GetComponent<MeshRenderer>();

        //Debug.Log("centro: "+s2.bounds.center);
        //Debug.Log("extents: "+s2.bounds.extents);
        //Debug.Log("diferencia: "+(s2.bounds.center - (s1.bounds.extents/ 2)));

        vasijas = new Vector3[cantVasijas - 1];
        aranas = new Vector3[cantAranas];
        aranas2 = new Vector3[cantAranas - 1];

        //POSICIONAR VASIJAS
        var posicionVasijaSala2 = sala2.transform.position + (Vector3.up * 0.5f);
        GameObject.Instantiate(prefabVasija, posicionVasijaSala2, Quaternion.identity);

        CalcularPosicionObjetos(vasijas, 0.5f, s1, distSeparacion);
        InstanciarObjetos(vasijas, prefabVasija);

        //POSICIONAR ARANAS
        //SALA 1
        CalcularPosicionObjetos(aranas, 3.0f, s1, distSeparacion);
        InstanciarObjetos(aranas, prefabAranaAux);

        /*
        //SALA 2
        distSeparacion = 2.0f;
        CalcularPosicionObjetos(aranas2, 8.0f, s2, distSeparacion);
        InstanciarObjetos(aranas2, prefabAranaAux);*/

        Debug.Log("volvio a arrancar por Juego");
        MomentoRunner.setCorrer(false);
    }


    private void CalcularPosicionObjetos(Vector3[] objetos, float altura, MeshRenderer sala, float distSeparacion)
    {
        int i = 0;

        while (i < objetos.Length)
        {
            dist = 100.0f;
            float x = Random.Range(sala.bounds.center.x - (sala.bounds.extents.x / 2), sala.transform.position.x + (sala.bounds.extents.x / 2));
            //float x = Random.Range(sala.transform.position.x - 15, sala.transform.position.x + 15);
            float y = altura;
            float z = Random.Range(sala.bounds.center.z - (sala.bounds.extents.z / 2), sala.transform.position.z + (sala.bounds.extents.z / 2));
            //float z = Random.Range(sala.transform.position.z - 15, sala.transform.position.z + 15);
            Vector3 posicion = new Vector3(x, y, z);



            int j = 0;
            while ((j < i) && (dist >= distSeparacion))
            {
                dist = Vector3.Distance(posicion, objetos[j]);
                //Debug.Log(dist);
                j++;
            }

            if (dist >= distSeparacion) {
                //Debug.Log("entro");
                objetos[i] = posicion;
                i++;
            }
        }
    }

    private void InstanciarObjetos(Vector3[] objetos, GameObject prefab)
    {
        for (int i = 0; i < objetos.Length; i++)
        {
            GameObject v = GameObject.Instantiate(prefab, objetos[i], Quaternion.identity);
        }

    }


    private void Update()
    {
        if (ConsiguioVasijas())
        {
            gano = true;
            
        }

        if (EntroSala2()) {
            //Debug.Log("entro Sala2");
            creadasSala2 = true;
            distSeparacion = 2.0f;
            CalcularPosicionObjetos(aranas2, -5.0f, s2, distSeparacion);
            InstanciarObjetos(aranas2, prefabAranaAux);
        }

        if (player.getPerdio())
        {
            perdio = true;
            tiempoCoolDownDerrota -= Time.deltaTime;
            tiempoCoolDownDerrota = Mathf.Max(0.0f, tiempoCoolDownDerrota);

            if (tiempoCoolDownDerrota == 0.0f)
            {
                //Debug.Log("Murio en Juego");
                gameover.gameObject.SetActive(true);
                menu.gameObject.SetActive(true);
            }
        }


    }

    private bool EntroSala2(){

        int layerPlayer = player.getLayer();
        string capa = LayerMask.LayerToName(layerPlayer);
        if ((capa == "sala2") && (!creadasSala2))
        {
            return true;
        }
        return false;

    }


    private bool ConsiguioVasijas()
    {
        int vasijasPlayer = player.getVasijas();
        if (vasijasPlayer == 3)
        {
            //Debug.Log("Gano");
            salida.SetActive(true);
            Destroy(puertaSalida);
            return true;
            
        }

        return false;
    }

    public static bool getGano()
    {
        return gano;
    }

    public static bool getPerdio()
    {
        return perdio;
    }

    public Vector3 MedidaSala(string sala)
    {
        if (sala =="sala1")
        {
            //return
        }

        return Vector3.zero;
    }
}
