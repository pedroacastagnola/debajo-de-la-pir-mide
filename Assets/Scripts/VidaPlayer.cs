﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaPlayer : MonoBehaviour
{
    public Player player;
    public Slider vidaPlayer;
    void Start()
    {
        vidaPlayer.maxValue = 100.0f;
        vidaPlayer.minValue = 0.0f;

    }

    
    void Update()
    {
        vidaPlayer.value =player.getVida();
    }
}
