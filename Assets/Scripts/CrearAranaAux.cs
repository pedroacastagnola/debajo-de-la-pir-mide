﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrearAranaAux : MonoBehaviour
{
    private bool isPiso1=false;
    public float distanciaPiso = 0.05f;
    public GameObject prefabArana;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //int layerPiso = LayerMask.GetMask("sala1");
        //isPiso1 = Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z), -transform.up, distanciaPiso, layerPiso);
        //isPiso1 = Physics.Raycast(new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), -transform.up, distanciaPiso, layerPiso);

        if (isPiso())
        {
            
            //Debug.Log("toco piso");
            Vector3 pos = gameObject.transform.position;
            Destroy(gameObject);
            GameObject v = GameObject.Instantiate(prefabArana, pos, Quaternion.identity);

        }
        else
        {
            //Debug.Log("no toca");
        }
    }

    private bool isPiso()
    {
        //int salas = 2;
        //int layerPiso = LayerMask.GetMask("sala1");
        //while (salas > 0)
        //{
            RaycastHit hit;
        //bool cayo = Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z), -transform.up, distanciaPiso, layerPiso);
        //if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z), -transform.up, out hit, distanciaPiso))
        if (Physics.Raycast(transform.position + transform.up * 0.1f, -transform.up, out hit, distanciaPiso))
        {
            //if (cayo)
            int layer = hit.collider.gameObject.layer;
            string capa = LayerMask.LayerToName(layer);
            if ((capa=="sala1") || (capa=="sala2")) {
                return true;
            }
            
            //layerPiso = LayerMask.GetMask("sala2");
            //salas--;
        }

        //}
        return false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position+transform.up*0.1f, transform.position - transform.up * distanciaPiso);
        //Gizmos.DrawLine(new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.position - transform.up * distanciaPiso);
        //Gizmos.DrawLine(new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), transform.position - transform.up * distanciaPiso);
    }
}
