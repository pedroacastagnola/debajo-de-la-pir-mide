﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomentoRunner : MonoBehaviour
{
    public Camera cam;
    private static bool correr = false;
    void Start()
    {
        cam = GetComponent<Camera>();
        Debug.Log("volvio a arrancar");
        correr = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("momento runner:"+correr);
            correr = true;
        }
    }

    /*private void FixedUpdate()
    {
        if (correr)
        {
            Debug.Log("entro");
            cam.transform.Rotate(0.0f, -0.90f, 0.0f, Space.Self);
        }
    }*/

    public static bool getCorrer()
    {
        return correr;
    }

    public static void setCorrer(bool estado)
    {
        correr =estado;
    }
}
