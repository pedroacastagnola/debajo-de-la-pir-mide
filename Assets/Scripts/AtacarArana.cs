﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtacarArana : MonoBehaviour
{
    private Collider arana=null;
    private bool encontrada=false;
    public GameObject prefabSangre;
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Arana"))
        {
            //Debug.Log("esta tocando");
            encontrada = true;
            arana = other;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            encontrada = false;
            arana = null;
        }
    }

    void PisarArana()
    {
        if (arana != null)
        {
            /*var cuerpo= arana.gameObject.transform.Find("mesh").gameObject;
            var sangre= arana.gameObject.transform.Find("Sangre").gameObject;

            cuerpo.SetActive(false);
            sangre.SetActive(true);*/

            GameObject.Destroy(arana.gameObject);
            GameObject.Instantiate(prefabSangre, arana.transform.position, prefabSangre.transform.rotation);

        }
    }
}
