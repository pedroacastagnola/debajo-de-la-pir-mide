﻿using System.Collections;
using System.Collections.Generic;
//using System.Numerics;
//using System.Numerics;
using UnityEngine;

public class Camara : MonoBehaviour
{
    private Camera cam;
    public Transform player;
    public float velCam = 4.0f;
    public float velCamRot = 300.0f;
    public float rotCamRunnerY = 60.0f;
    public float rotCamRunnerX = 30.0f;
    public float altura = 5.0f;
    public float offsetZ = 2.0f;
    public float offsetX = 5.0f;
    private bool correr = false;

    private bool gano = false;
    //private bool camaraRotada = false;
    private bool perdio = false;

    private AudioSource[] audios;
    public AudioClip musicaSinVasijas;
    public AudioClip musicaConVasijas;
    public AudioClip collapse;

    void Start()
    {
        cam = GetComponent<Camera>();
        audios = GetComponents<AudioSource>();
        audios[0].clip =musicaSinVasijas;
        audios[0].Play();
        correr = false;
        gano = false;
        perdio = false;
    }

    private void Update()
    {
        correr = MomentoRunner.getCorrer();

        gano = Juego.getGano();
        if ((gano) &&(audios[0].clip!=musicaConVasijas))
        {
            audios[0].clip = musicaConVasijas;
            audios[0].Play();

            audios[1].clip = collapse;
            audios[1].Play();
        }

        perdio = Juego.getPerdio();
    }

    void FixedUpdate()
    {
        if (!correr)
        {
            Vector3 objetivo = player.position + (Vector3.up * altura) - (Vector3.forward * offsetZ);
            transform.position = Vector3.Lerp(transform.position, objetivo, velCam * Time.deltaTime);
            if (gano)
            {
                var posCentro = Camera.main.transform.position;
                Camera.main.transform.position = posCentro + Random.insideUnitSphere * 0.2f;
            }
        }
        else
        {
            if (!perdio)
            {
                Quaternion objetivoRot = Quaternion.Euler(rotCamRunnerX, -rotCamRunnerY, 0);
                transform.rotation = Quaternion.Slerp(transform.rotation, objetivoRot, velCamRot * Time.deltaTime);
                Vector3 objetivo = player.position + (Vector3.up * (altura - 1.5f)) + (Vector3.right * offsetX);
                transform.position = Vector3.Lerp(transform.position, objetivo, velCam * Time.deltaTime);

                var posCentro = Camera.main.transform.position;
                Camera.main.transform.position = posCentro + Random.insideUnitSphere * 0.1f;

            }
        }

        if (perdio)
        {
            var posicion = transform.position;
            var rotacion = transform.rotation;

            transform.position = posicion;
            transform.rotation = rotacion;
        }
    }


}
