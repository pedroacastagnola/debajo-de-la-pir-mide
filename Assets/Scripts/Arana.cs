﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Arana : MonoBehaviour
{
    private NavMeshAgent agente;
    private Transform objetivo;
    private Animator anim;
    private Player player;
    public float distanciaPiso = 0.1f;
    private int layer=0;
    //private MeshRenderer s1;
    private MeshRenderer sala;

    void Start()
    {
        //s1= sala1. GetComponent<MeshRenderer>();

        anim = GetComponent<Animator>();
        objetivo = GameObject.FindGameObjectWithTag("Player").transform;
        agente = GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if(Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z), -transform.up,out hit, distanciaPiso))
        {
            layer = hit.collider.gameObject.layer;
            sala = hit.collider.gameObject.GetComponent<MeshRenderer>();
            //Debug.Log(LayerMask.LayerToName(layer));
        }

        Player player = objetivo.gameObject.GetComponent<Player>();
        int layerPlayer = player.getLayer();


        if ((agente != null) && (layer == layerPlayer))
        {
            agente.SetDestination(objetivo.position);
            //Debug.Log("arana: " + layer);
            //Debug.Log("jug: " + layerPlayer);
            //Debug.Log("coinciden");
        }
        else
        {
            //Debug.Log("ya no coinciden");
            //Debug.Log("arana: "+ layer);
            //Debug.Log("jug: "+layerPlayer);
            if (sala != null)
            {
                agente.SetDestination(sala.bounds.center);
            }

        }



    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.position - transform.up * distanciaPiso);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //Debug.Log("esta tocando");
            anim.SetBool("encontrado",true);
            player = other.gameObject.GetComponent<Player>();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            anim.SetBool("encontrado", false);
        }
    }

    void Atacar()
    {
        Debug.Log("entro");
        if (player != null)
        {
            player.restarVida();
        }
        else
        {
            Debug.Log("no econtrado");
        }
    }

}
