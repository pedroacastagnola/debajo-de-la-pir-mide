﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPrincipal : MonoBehaviour
{
    public GameObject ayuda;
    public GameObject menu;
    // Start is called before the first frame update
    public void OnJugar()
    {
        // Application.LoadLevel("Juego");
        SceneManager.LoadScene("Juego");
    }

    public void onAyuda()
    {
        ayuda.SetActive(true);
        menu.SetActive(false);
    }

    public void onVolver()
    {
        ayuda.SetActive(false);
        menu.SetActive(true);
    }
}
